<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Zabaat Inc | Technology Development Firm | Eugene / Creswell, OR</title>
<link rel="icon" href="favicon.ico" sizes="32x32" type="image/ico">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Zabaat Inc are professional custom software and systems developers. Our focus is machine to machine (M2M) technology and have a vision of an Internet of Things (IOT). We also enjoy web projects and flexing our design muscles.">
<meta name="keywords" content="eugene development, m2m, iot, software, IT, systems">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript">
var w = $(window).width();
var h = $(window).height();
var current_index = <?php echo $loaded_index; ?>;
var current_sub_index = 0; // index of sub page
var menu_hex_pos = ["18px","165px","308px","453px","599px"]
var pages = [["home"],["skills"],["work","work_dhc","work_nyc","work_cf","work_copic","work_vault"],["about","about_brett","about_brandon","about_alyssa","about_shahan","about_jared","about_goals","about_process"],["contact"]]
var inprocess = false;

$(function()
{
	var ele = $("[data-index=" + current_index + "]");
	ele.show();
	ele.css("left","0px"); // set current page left to 0px
	$("#menu_hex").css("left",menu_hex_pos[current_index]); // set the hexagon position
	var left = 0;
	var modifier = 1;
	var this_index = current_index + 1;
	for (var i=0; i<4; i++) // loop once for every other page
	{
		ele = $("[data-index=" + this_index + "]");
		if (this_index > 4) // for all pages after the loaded page, make the left a positive number equal to the body width; for all others, make the left a negative
		{
			this_index = 0;
			modifier = -1;
		}
		left = w * modifier;
		ele.css("left",left + "px");
		this_index++;
	}
	
	load_subs();
});

$.ajaxQ = (function(){
  var id = 0, Q = {};
  $(document).ajaxSend(function(e, jqx){
    jqx._id = ++id;
    Q[jqx._id] = jqx;
  });
  $(document).ajaxComplete(function(e, jqx){
    delete Q[jqx._id];
  });
  return {
    abortAll: function(){
      var r = [];
      $.each(Q, function(i, jqx){
        r.push(jqx._id);
        jqx.abort();
      });
      return r;
    }
  };
})();

function nav(selected_index)
{
	if (!inprocess)
	{
		$.ajaxQ.abortAll(); // cancel all ajax request before switching pages so there are not slowly returning pages from the previous section getting added to the new section
		
		var prev_index = current_index;
		current_index = selected_index;
		
		$("#menu_hex").animate({left:menu_hex_pos[current_index]},500,"easeInOutQuad"); // move the hexagon
		// $("#menu_hex").transition({x:menu_hex_pos[current_index]},500,"easeInOutQuad"); // move the hexagon
		
		if (prev_index == current_index) {if (current_sub_index != 0) nav_sub(0);} // if we are navagating back to the main page of the section, do a nav_sub
		else
		{
			var new_ele = $("[data-index=" + current_index + "]");
			
			var url = (current_index==0) ? "" : $(new_ele).attr("id");
			set_history(url);
			
			if (current_sub_index == 0) // navigating from main to main
			{
				var prev_ele = $("[data-index=" + prev_index + "]");
				var modifier = (current_index > prev_index) ? -1 : 1;
				var xleft = w * modifier;
				
				inprocess=true;
				new_ele.show().animate({left:"0px"},500,"easeInOutQuad");
				prev_ele.animate({left:xleft + "px"},500,"easeInOutQuad",function(){prev_ele.hide();inprocess=false;load_subs();set_page_lefts();});
				// new_ele.show().transition({left:0},500,"easeInOutQuad");
				// prev_ele.transition({left:xleft},500,"easeInOutQuad",function(){prev_ele.hide();inprocess=false;load_subs();set_page_lefts();});				
				
			}
			else  // navigating from sub to main
			{
				var prev_ele = $("[data-sub_index=" + current_sub_index + "]");
				current_sub_index = 0;
				var modifier = (current_index > prev_index) ? -1 : 1;
				var xleft = w * modifier;
				
				inprocess=true;
				new_ele.show().animate({left:"0px"},500,"easeInOutQuad");
				prev_ele.animate({left:xleft + "px"},500,"easeInOutQuad",function(){prev_ele.hide();inprocess=false;load_subs();$("[data-index=" + prev_index + "]").css("top","0px");set_page_lefts();}); //
				// new_ele.show().transition({x:0},500,"easeInOutQuad");
				// prev_ele.transition({left:xleft},500,"easeInOutQuad",function(){prev_ele.hide();inprocess=false;load_subs();$("[data-index=" + prev_index + "]").css("top","0px");set_page_lefts();}); //
			}
		}
	}
}

function nav_sub(selected_sub_index)
{
	if (!inprocess)
	{
		var prev_sub_index = current_sub_index;
		current_sub_index = selected_sub_index;
		
		var new_ele = (current_sub_index==0) ? $("[data-index=" + current_index + "]") : $("[data-sub_index=" + current_sub_index + "]");
		
		// set_history($(new_ele).attr("id")); // to enable this.. we need to modify the page initialization code to set tops
		
		var prev_ele = (prev_sub_index==0) ? $("[data-index=" + current_index + "]") : $("[data-sub_index=" + prev_sub_index + "]");
		
		var modifier = (current_sub_index > prev_sub_index) ? -1 : 1;
		var ytop = h * modifier;
		
		inprocess=true;
		new_ele.show().animate({top:"0px"},500,"easeInOutQuad");
		prev_ele.animate({top:ytop + "px"},500,"easeInOutQuad",function(){prev_ele.hide();inprocess=false;set_page_tops();});
		// new_ele.show().transition({y:0},500,"easeInOutQuad");
		// prev_ele.transition({y:ytop},500,"easeInOutQuad",function(){prev_ele.hide();set_page_tops();});
	}
}

function set_history(x)
{
	// if (window.location.href.indexOf("www") > -1) history.pushState("data", "", "http://www.graywallsband.com/"+x);  // else 
	history.pushState("data", "", "http://new.zabaat.com/"+x);
}

function load_subs()
{
	if (pages[current_index].length > 1)
	{
		$('.sub_page').remove(); // remove all previously loaded sub pages
		
		var top = 0;
		var page_name = "";
		
		var sub_index = 0; // so the create_ele function will have the appropriate sub_index .. really though this was only neccessary before I switched to synchronous.. I could probably just use the for loop index now
		
		for (var i=1; i<pages[current_index].length; i++)
		{
			page_name = pages[current_index][i] + "_content.php";
			$.ajax({ // synchronous post so the pages are returned in the correct order (sometimes asynchrounous ajax would return the second page before the first)
				type: 'POST',
				url: page_name,
				success: function(data){sub_index++;create_ele(sub_index,data);},
				async:false
			});
		}
	}
}

function create_ele(sub_index,content)
{
	var ele = $("<div id='" + pages[current_index][sub_index] + "' class='page sub_page' data-sub_index=" + sub_index + " style='top:" + h + "px;'>" + content + "</div>");
	$(ele).appendTo("#content");
}

function set_page_lefts()
{
	// set tops of pages ealier in the menu than x  to above
	var i = current_index - 1;  // index starts at previous page
	var left = w * -1;
	while (i >= 0)
	{
		$("[data-index=" + i + "]").css("left", left + "px");
		i--;
	}
	
	// set tops of pages later in the menu than x  to below
	i = Number(current_index) + 1; // index starts at next page
	left = w;
	while (i <= 4)
	{
		$("[data-index=" + i + "]").css("left", left + "px");
		i++;
	}
}

function set_page_tops()
{
	// set tops of pages ealier in the menu than x  to above
	var i = current_sub_index - 1;  // index starts at previous page
	var top = h * -1;
	while (i >= 1)
	{
		$("[data-sub_index=" + i + "]").css("top", top + "px");
		i--;
	}
	
	// set tops of pages later in the menu than x  to below
	i = Number(current_sub_index) + 1; // index starts at next page
	top = h;
	while (i < pages[current_index].length)
	{
		$("[data-sub_index=" + i + "]").css("top", top + "px");
		i++;
	}
}

function send()
{
	if($("#botty").val()=="")
	{
		var valid=true;
		$("#nom").css("color","black");
		$("#company").css("color","black");
		$("#phone").css("color","black");
		$("#email").css("color","black");
		$("#msg").css("color","black");
		var nom=$("#nom").val();
		var company=$("#company").val();
		var phone=$("#phone").val();
		var email=$("#email").val();
		var msg=$("#msg").val();
		if (nom=="" || nom=="name")
		{
			$("#nom").val("name");
			$("#nom").css("color","red");
			valid=false;
		}
		if (company=="" || company=="company")
		{
			$("#company").val("company");
			$("#company").css("color","red");
			valid=false;
		}
		if (phone=="" || phone=="phone")
		{
			$("#phone").val("phone");
			$("#phone").css("color","red");
			valid=false;
		}
		if (email=="" || email=="email")
		{
			$("#email").val("email");
			$("#email").css("color","red");
			valid=false;
		}
		
		if (msg=="" || msg=="message")
		{
			$("#msg").val("message");
			$("#msg").css("color","red");
			valid=false;
		}
		
		if (valid)
		{
			$("#contact_form").html("sending..");
			$.post("/ajax.php",{mode:"contact",nom:nom,company:company,phone:phone,email:email,msg:msg},function(data){$("#contact_form").html(data);});
		}
	}
	else
	{
		console.log("are you human?");
	}
}
</script>
</head>
<body>
<div id="topbar"></div>
<div id=""><span style="color:#85C443;">zabaat</span> <span style="color:#E5E5E5;">inc</span></div>
<div id="sidebar">website design by zabaat inc &copy;2014</div>
<div id="content">