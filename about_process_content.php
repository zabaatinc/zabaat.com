 <div class="content">
	<div class="page_title">
		<h2>Process</h2>
	</div>
	<div style="guts">
		<div class="hexagon hex_blue_border" style="position:absolute;top:50px;left:50px;">
			<div style="margin: 17px 0 0 22px;">contact</div>
		</div>
		
	<div style="position:absolute;top:120px;left:174px;border-bottom:3px solid grey;width:20px;"></div>
		
		<div class="hexagon hex_purple_border" style="position:absolute;top:50px;left:194px;">
			<div style="margin: 17px 0 0 29px;">define</div>
		</div>
		
	<div style="position:absolute;top:120px;left:318px;border-bottom:3px solid grey;width:20px;"></div>
		
		<div class="hexagon hex_fuscia_border" style="position:absolute;top:50px;left:338px;">
			<div style="margin: 17px 0 0 18px;">research</div>
		</div>
		
	<div style="position:absolute;top:178px;left:237px;border-bottom:3px solid grey;width:110px;transform: rotate(-30deg);"></div>
		
		<div class="hexagon hex_red_border" style="position:absolute;top:170px;left:122px;">
			<div style="margin: 17px 0 0 26px;">design</div>
		</div>
		
	<div style="position:absolute;top:239px;left:246px;border-bottom:3px solid grey;width:20px;"></div>
		
		<div class="hexagon hex_orange_border" style="position:absolute;top:170px;left:265px;">
			<div style="margin: 17px 0 0 34px;">build</div>
		</div>
		
	<div style="position:absolute;top:239px;left:389px;border-bottom:3px solid grey;width:20px;"></div>
		
		<div class="hexagon hex_yellow_border" style="position:absolute;top:170px;left:409px;">
			<div style="margin: 17px 0 0 42px;">test</div>
		</div>
		
	<div style="position:absolute;top:301px;left:493px;border-bottom:3px solid grey;width:30px;transform: rotate(60deg);"></div>
		
		<div class="hexagon hex_green_border" style="position:absolute;top:296px;left:487px;">
			<div style="margin: 17px 0 0 24px;">release</div>
		</div>
	</div>
	<div class="up clickable" onclick="javascript:nav_sub(0);"></div>
</div>