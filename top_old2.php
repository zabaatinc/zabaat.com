<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Zabaat Inc | Technology Development Firm | Eugene / Creswell, OR</title>
<link rel="icon" href="favicon.ico" sizes="32x32" type="image/ico">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Zabaat Inc are professional custom software and systems developers. Our focus is machine to machine (M2M) technology and have a vision of an Internet of Things (IOT). We also enjoy web projects and flexing our design muscles.">
<meta name="keywords" content="eugene development, m2m, iot, software, IT, systems">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript">
var w = $(window).width();
var current_index = <?php echo $loaded_index; ?>;
var current_page_id = "";
var menu_hex_pos = ["11px","161px","308px","451px","598px"]
// var pages = [["home"],["skills"],["work","work_dhc","work_nyc"],["about","brett","brandon","alyssa","shahan","jared"],["contact"]]

$(function()
{
	init();
	
});

function init()
{
	var ele = $("[data-index=" + current_index + "]");
	current_page_id = ele.attr("id");
	ele.css("left","0px"); // set current page left to 0px
	var left = 0;
	var modifier = 1;
	var this_index = current_index + 1;
	for (var i=0; i<4; i++) // loop once for every other page
	{
		ele = $("[data-index=" + this_index + "]");
		if (this_index > 4) // for all pages after the loaded page, make the left a positive number equal to the body width; for all others, make the left a negative
		{
			this_index = 0;
			modifier = -1;
		}
		left = w * modifier;
		ele.css("left",left + "px");
		this_index++;
	}
}

function nav(selected_id)
{
	var prev_ele = $("#" + current_page_id);
	current_page_id = selected_id;
	var new_ele = $("#" + selected_id);
	var new_index = new_ele.attr("data-index");
	var modifier = (new_index > prev_ele.attr("data-index")) ? -1 : 1;
	var xleft = w * modifier;

	$("#menu_hex").animate({left:menu_hex_pos[new_index]},300,"easeInOutQuad");
	new_ele.animate({left:"0px"},300,"easeInOutQuad");
	prev_ele.animate({left:xleft + "px"},300,"easeInOutQuad",function(){set_page_lefts(new_index);});
}

function set_page_lefts(x)
{
	// set tops of pages ealier in the menu than x  to above
	var i = x - 1;  // index starts at previous page
	var left = w * -1;
	while (i >= 0)
	{
		$("[data-index=" + i + "]").css("left", left + "px");
		i--;
	}
	
	// set tops of pages later in the menu than x  to below
	i = Number(x) + 1; // index starts at next page
	left = w;
	while (i <= 4)
	{
		$("[data-index=" + i + "]").css("left", left + "px");
		i++;
	}
}

function send()
{
	if($("#botty").val()=="")
	{
		var valid=true;
		$("#nom").css("color","black");
		$("#company").css("color","black");
		$("#phone").css("color","black");
		$("#email").css("color","black");
		$("#msg").css("color","black");
		var nom=$("#nom").val();
		var company=$("#company").val();
		var phone=$("#phone").val();
		var email=$("#email").val();
		var msg=$("#msg").val();
		if (nom=="" || nom=="name")
		{
			$("#nom").val("name");
			$("#nom").css("color","red");
			valid=false;
		}
		if (company=="" || company=="company")
		{
			$("#company").val("company");
			$("#company").css("color","red");
			valid=false;
		}
		if (phone=="" || phone=="phone")
		{
			$("#phone").val("phone");
			$("#phone").css("color","red");
			valid=false;
		}
		if (email=="" || email=="email")
		{
			$("#email").val("email");
			$("#email").css("color","red");
			valid=false;
		}
		
		if (msg=="" || msg=="message")
		{
			$("#msg").val("message");
			$("#msg").css("color","red");
			valid=false;
		}
		
		if (valid)
		{
			$("#contact_form").html("sending..");
			$.post("/ajax.php",{mode:"contact",nom:nom,company:company,phone:phone,email:email,msg:msg},function(data){$("#contact_form").html(data);});
		}
	}
	else
	{
		console.log("are you human?");
	}
}
</script>
</head>
<body>