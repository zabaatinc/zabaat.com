<div class="content">
	<div class="page_title">
		<h2>our skills and expertise</h2>
		<h4>these are all the cool things our team can do for you.</h4>
	</div>
	<div style="guts">
		<div class="col">
			<div>internet of things (IOT)</div>
			<div class="line"></div>
			<div>machine to machine</div>
			<div class="line"></div>
			<div>human - machine interface (HMI)</div>
			<div class="line"></div>
			<div>hardware design</div>
			<div class="line"></div>
			<div>UI / UX / print design</div>
			<div class="line"></div>
			<div>project management</div>
			<div class="line"></div>
			<div>application development</div>
			<div class="line"></div>
			<div>visual web design</div>
			<div class="line"></div>
			<div>responsive web design</div>
			<div class="line"></div>
			<div>web application development</div>
			<div class="line"></div>
			<div>mobile application development</div>
			<div class="line"></div>
			<div>IT management</div>
			<div class="line"></div>
			<div>branding &amp; identity</div>
			<div class="line"></div>
		</div>
		<div class="col">
			<div>vision</div>
			<div class="line"></div>
			<div>BT&nbsp;LE&nbsp;/&nbsp;CANbus&nbsp;/&nbsp;J1939&nbsp;/&nbsp;Modbus&nbsp;TCP</div>
			<div class="line"></div>
			<div>Qt 5.3 / QML</div>
			<div class="line"></div>
			<div>eagle / CAD</div>
			<div class="line"></div>
			<div>Adobe Illustrator / Photoshop</div>
			<div class="line"></div>
			<div>JIRA / Bitbucket / SourceTree</div>
			<div class="line"></div>
			<div>C++ / C#</div>
			<div class="line"></div>
			<div>HTML5 / CSS3</div>
			<div class="line"></div>
			<div>JavaScript / JQuery</div>
			<div class="line"></div>
			<div>PHP / MySQL</div>
			<div class="line"></div>
			<div>Java / Android</div>
			<div class="line"></div>
			<div>Windows / Linux Server</div>
			<div class="line"></div>
			<div>creativity / cleverness</div>
			<div class="line"></div>
		</div>
		<div class="clearer"></div>
	</div>
</div>