<?php
	include "top.php";
	include "menu.php";
		
	// must have all the menu pages loaded since they can be called any time.
	echo "<div id='home' class='page' data-index=0>"; include "home_content.php"; echo "</div>";
	echo "<div id='skills' class='page' data-index=1>"; include "skills_content.php"; echo "</div>"; //style='background-size: cover;background-image: url(images/dave.jpg);'
	echo "<div id='work' class='page' data-index=2>"; include "work_content.php"; echo "</div>";
	echo "<div id='about' class='page' data-index=3>"; include "about_content.php"; echo "</div>"; // style='background-size: cover;background-image: url(images/fish.jpg);'
	echo "<div id='contact' class='page' data-index=4>"; include "contact_content.php"; echo "</div>";
		
	include "bottom.php";
?>